from django import forms
from schedule_calendar.models import Preacher, Church, Event
from bootstrap_datepicker_plus import DatePickerInput
from django.forms import modelformset_factory
from crispy_forms.helper import FormHelper

class PreacherUpdateForm(forms.ModelForm):
    first_name = forms.CharField()
    last_name = forms.CharField()
    phone_no = forms.CharField()
    email = forms.EmailField()
    num_services = forms.IntegerField()
    availability = forms.DateField(
        widget=DatePickerInput(
            options={
                "format": "mm/dd/yyyy",
            }
        )
    )

    class Meta:
        model = Preacher
        fields = ['first_name', 'last_name', 'phone_no', 'email', 'availability']


# This form is for the drop down cells on the schedule table
class EventUpdateForm(forms.ModelForm):
    title = forms.CharField(label='', required=False, widget=forms.TextInput(attrs={'placeholder': 'Details'}))
    preacher = forms.ModelChoiceField(queryset=Preacher.objects.all(), label='', required=False)
    # Have to send the other variables as hidden fields for validation purposes - definitely not best practice
    id = forms.IntegerField(widget=forms.HiddenInput())

    # self call to exclude preachers from the selectable list who are not available
    def __init__(self, *args, **kwargs):
        super(EventUpdateForm, self).__init__(*args, **kwargs)
        event = self.instance
        self.fields['preacher'].queryset = Preacher.objects.exclude(availability__contains=[event.date]).order_by('last_name')
        # crispy forms helper function for assigning classes etc. to forms
        self.helper = FormHelper()
        self.helper.form_class = 'cell-forms'

    # affects what happens when you call form.save() or formset.save()
    class Meta:
        model = Event
        fields = ['id', 'title', 'preacher']

    field_order = ['id', 'title', 'preacher']


