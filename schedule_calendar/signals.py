from django.db.models.signals import post_save
from .models import Schedule, Event, Church
from django.dispatch import receiver
from datetime import datetime, timedelta
from . import utils
import calendar

# This function automatically fills the 'sundays' list every time a new Schedule is created.
@receiver(post_save, sender=Schedule)
def fill_sundays(sender, instance, created, **kwargs):
    if created:
        start_date = datetime
        end_date = datetime
        if instance.quarter == 2:
            start_date = datetime(instance.year-1, 12, 1) # Q2 begins in previous year's December
            end_date = datetime(instance.year, 2, calendar.monthrange(instance.year, 2)[1]) # Accounts for leap years
        elif instance.quarter == 3:
            start_date = datetime(instance.year, 3, 1)
            end_date = datetime(instance.year, 5, 31)
        elif instance.quarter == 4:
            start_date = datetime(instance.year, 6, 1)
            end_date = datetime(instance.year, 8, 31)
        elif instance.quarter == 1:
            start_date = datetime(instance.year, 9, 1)
            end_date = datetime(instance.year, 11, 30)

        delta = timedelta(days=1)
        while start_date <= end_date:
            if utils.isSunday(start_date):
                instance.sundays.append(start_date)
            start_date += delta
        instance.save()        # The instance is the schedule

# This function automatically creates some default events for each Church, on each Sunday, when a user creates a new Schedule
@receiver(post_save, sender=Schedule)
def create_events(sender, instance, created, **kwargs):
    if created:
        for date in instance.sundays:
            for church in Church.objects.all():
                if church.afternoon_service_time:
                    Event.objects.create(title="",
                                         schedule=instance,
                                         date=date,
                                         church=church,
                                         service_type='Morning')
                    Event.objects.create(title="",
                                         schedule=instance,
                                         date=date,
                                         church=church,
                                         service_type='Afternoon')
                else:
                    Event.objects.create(title="",
                                         schedule=instance,
                                         date=date,
                                         church=church,
                                         service_type='Morning')
                    Event.objects.create(title="No Service",
                                         schedule=instance,
                                         date=date,
                                         church=church,
                                         service_type='Afternoon')
