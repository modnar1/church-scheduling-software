from django.contrib import admin
from django.urls import path
from .views import ScheduleListView, ScheduleCreateView, ScheduleDeleteView, \
    PreacherListView, PreacherUpdateView, PreacherCreateView, PreacherDeleteView, \
    ChurchListView, ChurchUpdateView, ChurchCreateView, ChurchDeleteView, \
    EventListView, EventUpdateView, EventCreateView, EventDeleteView
from . import views

urlpatterns = [
    path('', ScheduleListView.as_view(), name='schedules'),
    path('schedules/<int:schedule_id>/view/', views.schedule_update, name='schedule_update'),
    path('schedules/edit/', views.get_form, name='get_form'),
    path('schedules/<int:schedule_id>/export/', views.excel_view, name='excel_view'),
    path('schedules/<int:pk>/delete/', ScheduleDeleteView.as_view(), name='schedule_delete'),
    path('schedules/new/', ScheduleCreateView.as_view(), name='schedule_create'),
    path('preacher_list/new/', PreacherCreateView.as_view(), name='preacher_form'),
    path('preacher_list/<int:pk>/update/', PreacherUpdateView.as_view(), name='preacher_update'),
    path('preacher_list/<int:pk>/delete/', PreacherDeleteView.as_view(), name='preacher_delete'),
    path('preacher_list/', PreacherListView.as_view(), name='preacher_list'),
    path('church_list/new/', ChurchCreateView.as_view(), name='church_form'),
    path('church_list/<int:pk>/update/', ChurchUpdateView.as_view(), name='church_update'),
    path('church_list/<int:pk>/delete/', ChurchDeleteView.as_view(), name='church_delete'),
    path('church_list/', ChurchListView.as_view(), name='church_list'),
    path('event_list/new/', EventCreateView.as_view(), name='event_form'),
    path('event_list/<int:pk>/update/', EventUpdateView.as_view(), name='event_update'),
    path('event_list/<int:pk>/delete/', EventDeleteView.as_view(), name='event_delete'),
    path('event_list/', EventListView.as_view(), name='event_list'),
]
