import django
import xlwt
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.forms import modelformset_factory
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import ListView, UpdateView, CreateView, DeleteView

from .forms import EventUpdateForm
from .models import Preacher, Church, Event, Schedule
# Create your views here.
from .scheduler import autoSchedule


#This function handles the conversion from HTML table to .xls spreadsheet
def excel_view(request, schedule_id):
    normal_style = xlwt.easyxf("""
     font:
         name Verdana
     """)
    response = HttpResponse(content_type='application/ms-excel')
    wb = xlwt.Workbook()
    ws0 = wb.add_sheet('Worksheet')
    ws0.write(0, 0, "something", normal_style)
    wb.save(schedule_id.xls)
    return response


# This function receives the AJAX request from the schedule update page and serves the correct form for the selected event
def get_form(request):
    if request.is_ajax():
        event = Event.objects.get(pk=request.GET.get('event_id'))
        print(event)
        form = EventUpdateForm(instance=event)
        return HttpResponse(form, content_type='text/html')
    else:
        return HttpResponse("no-go")


@login_required
def schedule_update(request, schedule_id):
    schedule = Schedule.objects.get(pk=schedule_id)
    churches = Church.objects.all().order_by('name')
    events = Event.objects.filter(schedule=schedule).order_by('date')

    EventFormSet = modelformset_factory(Event, form=EventUpdateForm, fields={'id', 'title', 'preacher'},
                                        extra=0)

    context = {
        'schedule': schedule,
        'churches': churches,
        'events': events,
        'formset': EventFormSet(queryset=Event.objects.none())
    }

    if request.method == 'POST':
        # If there's an error in input data, the data will remain in the form when the page is refreshed
        print(request.POST)
        formset = EventFormSet(request.POST)
        if formset.is_valid():
            # Do stuff with sent data depending on which button was clicked
            if request.POST.get("save"):
                formset.save()
                messages.success(request, f'Schedule {schedule} updated!')
                return HttpResponseRedirect(request.path_info)
            elif request.POST.get("auto_schedule"):
                # Always save the schedule's current state before assignment
                formset.save()
                result = autoSchedule(schedule)
                if result == "Auto-schedule completed successfully!":
                    messages.success(request, result)
                else:
                    messages.warning(request, result)
                return HttpResponseRedirect(request.path_info)
        else:
            print("Formset errors:")
            print(formset.errors)
    else:
        formset = EventFormSet(queryset=Event.objects.none())
        context['formset'] = formset
    return render(request, 'schedule_calendar/schedule_form.html', context)


class ScheduleListView(LoginRequiredMixin, ListView):
    template_name = 'schedule_calendar/schedule_list.html'
    model = Schedule
    context_object_name = 'schedules'  # Lets the class know in the template what the object is called.
    paginate_by = 10
    ordering = ['-year']


class ScheduleCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    template_name = 'schedule_calendar/schedule_create.html'
    model = Schedule
    fields = ['quarter', 'year']
    success_url = reverse_lazy('schedules')
    success_message = f'Schedule details saved successfully.'


class ScheduleDeleteView(SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    model = Schedule
    success_url = reverse_lazy('schedules')
    success_message = f'Schedule Deleted.'


class PreacherListView(LoginRequiredMixin, ListView):
    template_name = 'schedule_calendar/preacher_list.html'
    model = Preacher
    context_object_name = 'preachers'
    ordering = ['last_name']
    paginate_by = 10


class PreacherUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Preacher
    fields = ['first_name', 'last_name', 'email', 'phone_number', 'num_services', 'availability']
    success_url = reverse_lazy('preacher_list')
    success_message = f'Preacher details saved successfully.'


class PreacherCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Preacher
    fields = ['first_name', 'last_name', 'email', 'phone_number', 'num_services', 'availability']
    success_url = reverse_lazy('preacher_list')
    success_message = f'Preacher details saved successfully.'


class PreacherDeleteView(SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    model = Preacher
    success_url = reverse_lazy('preacher_list')
    success_message = f'Preacher Deleted.'


class ChurchListView(LoginRequiredMixin, ListView):
    template_name = 'schedule_calendar/church_list.html'
    model = Church
    context_object_name = 'churches'
    ordering = ['name']
    paginate_by = 10


class ChurchUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Church
    fields = ['name', 'number', 'address', 'morning_service_time', 'afternoon_service_time']
    success_url = reverse_lazy('church_list')
    success_message = f'Church details saved successfully.'

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['morning_service_time'].widget = django.forms.TimeInput(attrs={'type': 'time', 'value': '09:00:00'})
        form.fields['afternoon_service_time'].widget = django.forms.TimeInput(attrs={'type': 'time'})
        form.fields['afternoon_service_time'].label = 'Afternoon service time (optional)'
        return form


class ChurchCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Church
    fields = ['name', 'number', 'address', 'morning_service_time', 'afternoon_service_time']
    success_url = reverse_lazy('church_list')
    success_message = f'Church details saved successfully.'

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['morning_service_time'].widget = django.forms.TimeInput(attrs={'type': 'time', 'value': '09:00:00'})
        form.fields['afternoon_service_time'].widget = django.forms.TimeInput(attrs={'type': 'time'})
        form.fields['afternoon_service_time'].label = 'Afternoon service time (optional)'
        return form


class ChurchDeleteView(SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    model = Church
    success_url = reverse_lazy('church_list')
    success_message = f'Church Deleted.'


class EventListView(LoginRequiredMixin, ListView):
    template_name = 'schedule_calendar/event_list.html'
    model = Event
    context_object_name = 'events'
    ordering = ['date']
    paginate_by = 10


class EventUpdateView(SuccessMessageMixin, LoginRequiredMixin, UpdateView):
    model = Event
    fields = ['title', 'schedule', 'date', 'church', 'preacher', 'service_type']
    success_url = reverse_lazy('event_list')
    success_message = f'Event saved successfully.'


class EventCreateView(SuccessMessageMixin, LoginRequiredMixin, CreateView):
    model = Event
    fields = ['title', 'schedule', 'date', 'church', 'preacher', 'service_type']
    success_url = reverse_lazy('event_list')
    success_message = f'Event saved successfully.'


class EventDeleteView(SuccessMessageMixin, LoginRequiredMixin, DeleteView):
    model = Event
    success_url = reverse_lazy('event_list')
    success_message = f'Event Deleted.'
