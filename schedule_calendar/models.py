from collections import Counter

from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.core.validators import RegexValidator
from django.utils.datetime_safe import strftime, time, datetime


class Schedule(models.Model):
    # Q1: September, October, November
    # Q2: December, January, February
    # Q3: March, April, May
    # Q4: June, July, August
    CHOICES = [(1, 'Q1 September - November'),
               (2, 'Q2 December - February (begins in previous year)'),
               (3, 'Q3 March - May'),
               (4, 'Q4 June - August')]

    quarter = models.IntegerField(choices=CHOICES)
    year = models.PositiveIntegerField()
    sundays = ArrayField(models.DateField(), default=list, blank=True, null=True) # Generate a list of all Sundays in the quarter when a schedule is created

    class Meta:
        unique_together = ['quarter', 'year']

    # Annoyingly have to use this property tag as you can't call date format functions in django template
    #TODO: Generate these lists when a schedule is created to avoid having to do it every time page is loaded
    # Returns a dict with month string, and int length in cells for formatting the schedule table
    @property
    def months(self):
        monthlist = list()
        for sunday in self.sundays:
            monthlist.append(sunday.strftime('%B %Y'))
        months = Counter(monthlist)
        return dict(months)

    # Returns formatted days for display in schedule table
    @property
    def dates(self):
        dates = list()
        for sunday in self.sundays:
            dates.append(sunday.strftime('%d'))
        return dates

    def __str__(self):
        if self.quarter == 1:
            return "Q" + str(self.quarter) + " " + str(self.year-1) + "-" + str(self.year)
        else:
            return "Q" + str(self.quarter) + " " + str(self.year)

class Church(models.Model):
    name = models.CharField(max_length=100)
    number = models.PositiveIntegerField()
    address = models.CharField(max_length=400)
    # Hold time here, since the morning and afternoon services for each church
    # always have the same time, unless specified in the event title
    morning_service_time = models.TimeField(auto_now=False, auto_now_add=False)
    afternoon_service_time = models.TimeField(auto_now=False, auto_now_add=False, null=True, blank=True)

    # Essentially a toString method
    def __str__(self):
        return self.name


class Preacher(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                                 message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    email = models.EmailField(max_length=100, null=True, blank=True)
    num_services = models.PositiveIntegerField(null=True, blank=True, verbose_name="Number of services this preacher "
                                                                                   "would like to cover per quarter. "
                                                                                   "Leave blank to let the system determine a fair number.")
    availability = ArrayField(models.DateField(), default=list, blank=True)

    def getDates(self):
        dates = []
        for date in self.availability:
            dates.append(date.strftime("%m/%d/%Y"))
        return dates

    def __str__(self):
        if self.first_name == 'Local Arrangements':
            return self.first_name
        else:
            return self.first_name[0].upper() + '. ' + self.last_name


class Event(models.Model):

    # Hold service type here, since a service can only be either morning or afternoon
    class ServiceType(models.TextChoices):
        MORNING = 'Morning'
        AFTERNOON = 'Afternoon'

    title = models.CharField(max_length=100, blank=True, default="") # Most services will not actually have a title, they will be displayed on the schedule as the preacher's name
    schedule = models.ForeignKey(Schedule, null=True, blank=True, on_delete=models.CASCADE) # Delete all events in a schedule if schedule is deleted
    date = models.DateField()
    church = models.ForeignKey(Church, on_delete=models.CASCADE)
    preacher = models.ForeignKey(Preacher, null=True, blank=True,
                                 on_delete=models.SET_NULL)  # Field can be null when object is created
    service_type = models.CharField(
        max_length=9,
        choices=ServiceType.choices,
        default=ServiceType.MORNING,
    )

    def __str__(self):
        return str(self.id) + " | " + str(self.title) + " | " + str(self.date) + " | " + str(self.church) + " | " + str(self.preacher) + " | " + str(self.service_type)