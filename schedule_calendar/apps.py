from django.apps import AppConfig


class ScheduleCalendarConfig(AppConfig):
    name = 'schedule_calendar'

    def ready(self):
        import schedule_calendar.signals
        import schedule_calendar.scheduler
