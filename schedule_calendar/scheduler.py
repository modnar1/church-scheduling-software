# File to contain the logic for the auto-scheduler
import networkx as nx
from .models import Event, Preacher, Schedule


# This function generates a graph from a given schedule object for the purposes of solving the MinCost flow problem,
# then assigns preachers to events with the results. It returns a string that indicates the results of the assignment.
def autoSchedule(schedule):
    preachers = Preacher.objects.all()
    # Count all events that are not marked 'No Service'
    services = Event.objects.filter(schedule=schedule).exclude(title="No Service")
    numservices = len(services)

    # Create the graph
    graph = nx.MultiDiGraph(schedule=schedule)
    # First add source and sink nodes
    graph.add_nodes_from([
        ('source', {'demand': -numservices}),
        ('sink', {'demand': numservices}),
    ])
    # Add all preacher nodes
    graph.add_nodes_from(Preacher.objects.all())
    # Add a morning and afternoon node for each preacher on each day they are available
    for preacher in preachers:
        for sunday in schedule.sundays:
            if sunday not in preacher.availability:
                graph.add_node(str(preacher.first_name) + " " + str(preacher.last_name) + " Morning " + str(sunday), preacher=preacher, date=sunday,
                               service_type='Morning', preachernode=True)
                graph.add_node(str(preacher.first_name) + " " + str(preacher.last_name) + " Afternoon " + str(sunday), preacher=preacher, date=sunday,
                               service_type='Afternoon', preachernode=True)

    # Add nodes for all services (already filtered out 'No Service' titles)
    graph.add_nodes_from(services)

    # Determine a roughly fair number of services to be assigned to each preacher
    fairnum = int(numservices / len(preachers) + 1)

    # Connect the source to each preacher node with edges of increasing cost up to the fairnum (+ 3 for overflow)
    # If a preacher has a number of services they would like to do, use that, otherwise use the more general fairnum
    for preacher in preachers:
        if preacher.num_services is not None:
            for i in range(preacher.num_services):
                graph.add_edges_from([('source', preacher, {'cost': i * 100, 'capacity': 1})])
        else:
            for i in range(fairnum):
                graph.add_edges_from([('source', preacher, {'cost': i * 100, 'capacity': 1})])
        graph.add_edges_from([('source', preacher, {'cost': 1000000, 'capacity': 1})])
        graph.add_edges_from([('source', preacher, {'cost': 2000000, 'capacity': 1})])
        graph.add_edges_from([('source', preacher, {'cost': 3000000, 'capacity': 1})])

    # Select all the nodes which represent each preacher's assignments for a day
    selected_nodes = [
        node
        for node, attr in graph.nodes(data=True)
        if (attr.get('preachernode') == True)
    ]

    # Connect preacher nodes to these nodes
    for preachernode in selected_nodes:
        for preacher in preachers:
            if graph.nodes[preachernode]['preacher'] == preacher:
                graph.add_edge(preacher, preachernode, cost=100, capacity=1)

    # Connect each of these nodes to each church's morning and afternoon services, respectively
    for preachernode in selected_nodes:
        for service in services:
            if graph.nodes[preachernode]['preacher'] == service.preacher:
                # 0 cost to ensure a pre-assigned preacher is still assigned to this service
                graph.add_edge(preachernode, service, cost=0, capacity=1)
            elif graph.nodes[preachernode]['date'] == service.date and graph.nodes[preachernode][
                'service_type'] == service.service_type:
                graph.add_edge(preachernode, service, cost=100, capacity=1)

    # Connect the service nodes to the sink
    # TODO: Maybe find a better way to select the sink node, not sure how to select a single node in networkx
    selected_nodes = [
        node
        for node, attr in graph.nodes(data=True)
        if (node == 'sink')
    ]

    for service in services:
        graph.add_edge(service, selected_nodes[0], cost=100, capacity=1)

    # Final section of the function calculates the minimum cost flow from source to sink node, then extracts the
    # assignments from the generated data, assigning preachers to events
    try:
        flowCost = nx.min_cost_flow_cost(graph, weight='cost')
        flowDict = nx.min_cost_flow(graph, weight='cost')
        # the NX function returns a dict of dicts of dicts, so it requires a lot of unpacking to get at the relevant data
        sundays = schedule.sundays
        for k, v in flowDict.items():
            for preacher in preachers:
                for sunday in sundays:
                    if str(preacher.first_name) + " " + str(preacher.last_name) + " Morning " + str(sunday) == k or \
                            str(preacher.first_name) + " " + str(preacher.last_name) + " Afternoon " + str(sunday) == k:
                        for k2, v2 in v.items():
                            for k3, v3 in v2.items():
                                if v3 == 1:
                                    # have to go back to the bloody graph here to get the preacher object from its label
                                    assignedpreacher = graph.nodes.get(k)['preacher']
                                    assignedevent = k2
                                    # Print out for debugging
                                    # print(f"{assignedpreacher} is assigned to {assignedevent.church} on "
                                    #       f"{assignedevent.date} for the {assignedevent.service_type} service.")

                                    # Finally, assign the preachers to their assigned events and save to database
                                    assignedevent.preacher = assignedpreacher
                                    assignedevent.save()
                                    if flowCost > 1000000:  # extra edges have been taken to fill services
                                        result = f"Auto-schedule completed successfully! " \
                                                 f"Warning: Some preachers may be overburdened!"
                                    else:
                                        result = "Auto-schedule completed successfully!"
    except nx.exception.NetworkXUnfeasible:
        result = 'Graph error: There is no flow satisfying all demand'
    except nx.exception.NetworkXError:
        result = 'Graph error: Graph is not connected.'
    except:
        result = 'Unknown error: Something went wrong with the auto-scheduler.'
    return result
