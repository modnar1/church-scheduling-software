# File to contain any small utilities used elsewhere in the application

from datetime import datetime, date


# Small function to determine whether a datetime object corresponds to a Sunday or not


def isSunday(date):
    if date.weekday() == 6:
        return True
    else:
        return False
