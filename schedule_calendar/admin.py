from django.contrib import admin
from .models import Church, Preacher, Event, Schedule

# Register your models here. Shows up on admin page
admin.site.register(Church)
admin.site.register(Preacher)
admin.site.register(Event)
admin.site.register(Schedule)
